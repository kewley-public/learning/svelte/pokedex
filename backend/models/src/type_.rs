use rocket::serde::{Deserialize, Serialize};
use sea_orm::entity::prelude::*;

#[derive(Clone, Debug, PartialEq, Eq, DeriveEntityModel, Deserialize, Serialize, FromForm)]
#[serde(crate = "rocket::serde")]
#[sea_orm(table_name = "type")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub name: String,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(has_many = "super::pokemon_type::Entity")]
    PokemonType,
}

impl Related<super::pokemon_type::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::PokemonType.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}
