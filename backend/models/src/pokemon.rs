use crate::{my_date_format, UTC};
use rocket::form::{FromForm, FromFormField};
use rocket::serde::{Deserialize, Serialize};
use sea_orm::entity::prelude::*;
use std::fmt::Debug;

#[derive(Clone, Debug, PartialEq, Eq, DeriveEntityModel, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
#[sea_orm(table_name = "pokemon")]
pub struct Model {
    #[sea_orm(primary_key)]
    pub id: i32,
    pub name: String,
    #[sea_orm(column_type = "Text")]
    pub description: String,
    pub front_sprite_url: String,
    pub back_sprite_url: String,
    pub front_shiny_sprite_url: String,
    pub back_shiny_sprite_url: String,
    #[serde(with = "my_date_format")]
    pub last_sync: UTC,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(has_many = "super::pokemon_type::Entity")]
    PokemonType,
    #[sea_orm(has_many = "super::pokemon_move::Entity")]
    PokemonMove,
}

impl Related<super::type_::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::PokemonType.def()
    }
}

impl Related<super::move_::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::PokemonMove.def()
    }
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize, FromForm)]
#[serde(crate = "rocket::serde")]
pub struct CreatePokemonRequest {
    pub name: String,
    pub description: String,
    pub front_sprite_url: String,
    pub back_sprite_url: String,
    pub front_shiny_sprite_url: String,
    pub back_shiny_sprite_url: String,
    pub types: Vec<Type>,
    pub moves: Vec<Move>,
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct PaginatedPokemon {
    pub id: Option<i32>,
    pub name: String,
    pub description: String,
    pub front_sprite_url: String,
    pub back_sprite_url: String,
    pub front_shiny_sprite_url: String,
    pub back_shiny_sprite_url: String,
    #[serde(with = "my_date_format")]
    pub last_sync: UTC,
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize)]
#[serde(crate = "rocket::serde")]
pub struct PokemonWithDetails {
    pub id: Option<i32>,
    pub name: String,
    pub description: String,
    pub types: Vec<Type>,
    pub moves: Vec<Move>,
    pub front_sprite_url: String,
    pub back_sprite_url: String,
    pub front_shiny_sprite_url: String,
    pub back_shiny_sprite_url: String,
    #[serde(with = "my_date_format")]
    pub last_sync: UTC,
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize, FromForm)]
#[serde(crate = "rocket::serde")]
pub struct Type {
    pub id: Option<i32>,
    pub name: String,
}

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize, FromForm)]
#[serde(crate = "rocket::serde")]
pub struct Move {
    pub id: Option<i32>,
    pub name: String,
}

impl ActiveModelBehavior for ActiveModel {}
