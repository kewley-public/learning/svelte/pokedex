#[macro_use]
extern crate rocket;

use chrono::{NaiveDateTime, Utc};
use rocket::form::FromFormField;
use rocket::serde::{Deserialize, Serialize};

pub mod move_;
pub mod pokemon;
pub mod pokemon_move;
pub mod pokemon_type;
pub mod prelude;
pub mod type_;

pub use move_::Entity as Move;
pub use pokemon::Entity as Pokemon;
pub use pokemon_move::Entity as PokemonMove;
pub use pokemon_type::Entity as PokemonType;
pub use type_::Entity as Type;

pub mod my_date_format {
    use super::UTC;
    use rocket::serde::{Deserialize, Deserializer, Serializer};

    pub fn serialize<S>(date: &UTC, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let formatted = date.format("%Y-%m-%dT%H:%M:%S%.fZ").to_string();
        serializer.serialize_str(&formatted)
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<UTC, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s = String::deserialize(deserializer)?;
        UTC::parse_from_str(&s, "%Y-%m-%dT%H:%M:%S%.fZ").map_err(rocket::serde::de::Error::custom)
    }
}

pub type UTC = NaiveDateTime;

pub fn now() -> UTC {
    Utc::now().naive_utc()
}
