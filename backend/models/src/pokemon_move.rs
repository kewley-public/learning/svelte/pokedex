use rocket::serde::{Deserialize, Serialize};
use sea_orm::entity::prelude::*;

#[derive(Clone, Debug, PartialEq, Eq, DeriveEntityModel, Deserialize, Serialize, FromForm)]
#[serde(crate = "rocket::serde")]
#[sea_orm(table_name = "pokemon_move")]
pub struct Model {
    #[sea_orm(primary_key, auto_increment = false)]
    pub pokemon_id: i32,
    #[sea_orm(primary_key, auto_increment = false)]
    pub move_id: i32,
}

#[derive(Copy, Clone, Debug, EnumIter, DeriveRelation)]
pub enum Relation {
    #[sea_orm(
        belongs_to = "super::pokemon::Entity",
        from = "Column::PokemonId",
        to = "super::pokemon::Column::Id"
    )]
    Pokemon,
    #[sea_orm(
        belongs_to = "super::move_::Entity",
        from = "Column::MoveId",
        to = "super::move_::Column::Id"
    )]
    Move,
}

impl Related<super::pokemon::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Pokemon.def()
    }
}

impl Related<super::move_::Entity> for Entity {
    fn to() -> RelationDef {
        Relation::Move.def()
    }
}

impl ActiveModelBehavior for ActiveModel {}
