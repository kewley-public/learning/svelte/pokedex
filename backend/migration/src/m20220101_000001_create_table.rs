use sea_orm_migration::{prelude::*, schema::*};

#[derive(DeriveMigrationName)]
pub struct Migration;

#[async_trait::async_trait]
impl MigrationTrait for Migration {
    async fn up(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        // Create the `pokemon` table
        manager
            .create_table(
                Table::create()
                    .table(Pokemon::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Pokemon::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Pokemon::Name).string().not_null())
                    .col(ColumnDef::new(Pokemon::Description).string().not_null())
                    .col(ColumnDef::new(Pokemon::FrontSpriteUrl).string().not_null())
                    .col(ColumnDef::new(Pokemon::BackSpriteUrl).string().not_null())
                    .col(
                        ColumnDef::new(Pokemon::FrontShinySpriteUrl)
                            .string()
                            .not_null(),
                    )
                    .col(
                        ColumnDef::new(Pokemon::BackShinySpriteUrl)
                            .string()
                            .not_null(),
                    )
                    .col(ColumnDef::new(Pokemon::LastSync).timestamp().not_null())
                    .to_owned(),
            )
            .await?;

        // Create the `types` table
        manager
            .create_table(
                Table::create()
                    .table(Type::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Type::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Type::Name).string().not_null())
                    .to_owned(),
            )
            .await?;

        // Create the `pokemon_types` join table
        manager
            .create_table(
                Table::create()
                    .table(PokemonType::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(PokemonType::PokemonId).integer().not_null())
                    .col(ColumnDef::new(PokemonType::TypeId).integer().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .from(PokemonType::Table, PokemonType::PokemonId)
                            .to(Pokemon::Table, Pokemon::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(PokemonType::Table, PokemonType::TypeId)
                            .to(Type::Table, Type::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await?;

        // Create the `moves` table
        manager
            .create_table(
                Table::create()
                    .table(Move::Table)
                    .if_not_exists()
                    .col(
                        ColumnDef::new(Move::Id)
                            .integer()
                            .not_null()
                            .auto_increment()
                            .primary_key(),
                    )
                    .col(ColumnDef::new(Move::Name).string().not_null())
                    .to_owned(),
            )
            .await?;

        // create the `pokemon_moves` join table
        manager
            .create_table(
                Table::create()
                    .table(PokemonMove::Table)
                    .if_not_exists()
                    .col(ColumnDef::new(PokemonMove::PokemonId).integer().not_null())
                    .col(ColumnDef::new(PokemonMove::MoveId).integer().not_null())
                    .foreign_key(
                        ForeignKey::create()
                            .from(PokemonMove::Table, PokemonMove::PokemonId)
                            .to(Pokemon::Table, Pokemon::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .foreign_key(
                        ForeignKey::create()
                            .from(PokemonMove::Table, PokemonMove::MoveId)
                            .to(Move::Table, Move::Id)
                            .on_delete(ForeignKeyAction::Cascade),
                    )
                    .to_owned(),
            )
            .await?;

        Ok(())
    }

    async fn down(&self, manager: &SchemaManager) -> Result<(), DbErr> {
        manager
            .drop_table(Table::drop().table(PokemonType::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(PokemonMove::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Type::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Move::Table).to_owned())
            .await?;
        manager
            .drop_table(Table::drop().table(Pokemon::Table).to_owned())
            .await?;
        Ok(())
    }
}

#[derive(DeriveIden)]
enum Pokemon {
    Table,
    Id,
    Name,
    Description,
    FrontSpriteUrl,
    BackSpriteUrl,
    FrontShinySpriteUrl,
    BackShinySpriteUrl,
    LastSync,
}

#[derive(DeriveIden)]
enum Type {
    Table,
    Id,
    Name,
}

#[derive(DeriveIden)]
enum PokemonType {
    Table,
    PokemonId,
    TypeId,
}

#[derive(DeriveIden)]
enum Move {
    Table,
    Id,
    Name,
}

#[derive(DeriveIden)]
enum PokemonMove {
    Table,
    PokemonId,
    MoveId,
}
