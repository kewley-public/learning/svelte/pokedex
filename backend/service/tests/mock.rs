mod prepare;

use entity::pokemon;
use prepare::prepare_mock_db;
use service::{Mutation, Query};

#[tokio::test]
async fn main() {
    let db = &prepare_mock_db();

    {
        let pokemon = Query::find_pokemon_by_id(db, 1).await.unwrap().unwrap();

        assert_eq!(pokemon.id, 1);
    }

    {
        let pokemon = Query::find_pokemon_by_id(db, 5).await.unwrap().unwrap();

        assert_eq!(pokemon.id, 5);
    }

    {
        let pokemon = Mutation::create_pokemon(
            db,
            pokemon::Model {
                id: 0,
                name: "Name D".to_owned(),
                text: "Type D".to_owned(),
            },
        )
        .await
        .unwrap();

        assert_eq!(
            pokemon,
            pokemon::ActiveModel {
                id: sea_orm::ActiveValue::Unchanged(6),
                name: sea_orm::ActiveValue::Unchanged("Name D".to_owned()),
                text: sea_orm::ActiveValue::Unchanged("Type D".to_owned())
            }
        );
    }

    {
        let pokemon = Mutation::update_pokemon_by_id(
            db,
            1,
            pokemon::Model {
                id: 1,
                name: "New Name A".to_owned(),
                text: "New Type A".to_owned(),
            },
        )
        .await
        .unwrap();

        assert_eq!(
            pokemon,
            pokemon::Model {
                id: 1,
                name: "New Name A".to_owned(),
                text: "New Type A".to_owned(),
            }
        );
    }
}
