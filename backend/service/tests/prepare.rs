use ::entity::pokemon;
use sea_orm::*;

#[cfg(feature = "mock")]
pub fn prepare_mock_db() -> DatabaseConnection {
    MockDatabase::new(DatabaseBackend::Postgres).append_query_results([
        [pokemon::Model {
            id: 1,
            name: "Name A".to_owned(),
            text: "Type A".to_owned(),
        }],
        [pokemon::Model {
            id: 5,
            name: "Name C".to_owned(),
            text: "Type C".to_owned(),
        }],
        [pokemon::Model {
            id: 6,
            name: "Name D".to_owned(),
            text: "Type D".to_owned(),
        }],
        [pokemon::Model {
            id: 1,
            name: "Name A".to_owned(),
            text: "Type A".to_owned(),
        }],
        [pokemon::Model {
            id: 1,
            name: "New Name A".to_owned(),
            text: "New Type A".to_owned(),
        }],
        [pokemon::Model {
            id: 5,
            name: "Name C".to_owned(),
            text: "Type C".to_owned(),
        }],
    ])
}
