use models::prelude::*;
use sea_orm::query::*;
use sea_orm::*;

pub struct Query;

impl Query {
    pub async fn find_types_for_pokemon(db: &DbConn, id: i32) -> Result<PokemonModel, DbErr> {
        let pokemon = Pokemon::find_by_id(id)
            .find_with_related(Type)
            .all(db)
            .await?;

        Ok(pokemon.first().map(|(p, _)| p).cloned().unwrap())
    }

    pub async fn find_pokemon_by_id(
        db: &DbConn,
        id: i32,
    ) -> Result<Option<PokemonWithDetails>, DbErr> {
        let pokemon = Pokemon::find_by_id(id).one(db).await?;

        if let Some(pokemon) = pokemon {
            // Finding moves related to the Pokemon via the pokemon_move join table
            let moves = Move::find()
                .join(JoinType::InnerJoin, MoveRelation::PokemonMove.def())
                .filter(PokemonMoveColumn::PokemonId.eq(pokemon.id))
                .all(db)
                .await?;

            let types = Type::find()
                .join(JoinType::InnerJoin, TypeRelation::PokemonType.def())
                .filter(PokemonTypeColumn::PokemonId.eq(pokemon.id))
                .all(db)
                .await?;

            Ok(Some(PokemonWithDetails {
                id: Some(pokemon.id),
                name: pokemon.name,
                description: pokemon.description,
                types: types
                    .iter()
                    .map(|t| PokemonWithDetailsType {
                        id: Some(t.id),
                        name: t.name.clone(),
                    })
                    .collect(),
                moves: moves
                    .iter()
                    .map(|m| PokemonWithDetailsMove {
                        id: Some(m.id),
                        name: m.name.clone(),
                    })
                    .collect(),
                front_sprite_url: pokemon.front_sprite_url,
                back_sprite_url: pokemon.back_sprite_url,
                front_shiny_sprite_url: pokemon.front_shiny_sprite_url,
                back_shiny_sprite_url: pokemon.back_shiny_sprite_url,
                last_sync: pokemon.last_sync,
            }))
        } else {
            Ok(None)
        }
    }

    pub async fn find_pokemon_in_page(
        db: &DbConn,
        page: u64,
        pokemons_per_page: u64,
    ) -> Result<(Vec<PaginatedPokemon>, u64, u64), DbErr> {
        // Setup paginator
        let paginator = Pokemon::find()
            .order_by_asc(PokemonColumn::Id)
            .paginate(db, pokemons_per_page);
        let num_items_and_pages = paginator.num_items_and_pages().await?;

        // Fetch paginated pokemons and map to PaginatedPokemon
        paginator.fetch_page(page - 1).await.map(|p| {
            (
                p.iter()
                    .map(|p| PaginatedPokemon {
                        id: Some(p.id),
                        name: p.name.clone(),
                        description: p.description.clone(),
                        front_sprite_url: p.front_sprite_url.clone(),
                        back_sprite_url: p.back_sprite_url.clone(),
                        front_shiny_sprite_url: p.front_shiny_sprite_url.clone(),
                        back_shiny_sprite_url: p.back_shiny_sprite_url.clone(),
                        last_sync: p.last_sync.clone(),
                    })
                    .collect(),
                num_items_and_pages.number_of_pages,
                num_items_and_pages.number_of_items,
            )
        })
    }
}
