use futures::future::try_join_all;
use models::now;
use models::prelude::*;
use sea_orm::ActiveValue::Set;
use sea_orm::*;

pub struct Mutation;

impl Mutation {
    pub async fn create_pokemon(
        db: &DbConn,
        form_data: CreatePokemonRequest,
    ) -> Result<PokemonWithDetails, DbErr> {
        let mut pokemon = PokemonActiveModel {
            name: Set(form_data.name.to_owned()),
            description: Set(form_data.description.to_owned()),
            front_sprite_url: Set(form_data.front_sprite_url.to_owned()),
            back_sprite_url: Set(form_data.back_sprite_url.to_owned()),
            front_shiny_sprite_url: Set(form_data.front_shiny_sprite_url.to_owned()),
            back_shiny_sprite_url: Set(form_data.back_shiny_sprite_url.to_owned()),
            last_sync: Set(now()),
            ..Default::default()
        }
        .insert(db)
        .await?;

        let txn = db.begin().await?;

        let type_futures = form_data.types.iter().map(|t| async {
            match Type::find()
                .filter(TypeColumn::Name.eq(&t.name.clone()))
                .one(db)
                .await?
            {
                Some(t) => Ok(t.id),
                None => TypeActiveModel {
                    name: Set(t.name.clone()),
                    ..Default::default()
                }
                .insert(db)
                .await
                .map(|res| res.id),
            }
        });

        let type_ids = try_join_all(type_futures).await?;

        let move_futures = form_data.moves.iter().map(|m| async {
            match Move::find()
                .filter(MoveColumn::Name.eq(&m.name.clone()))
                .one(db)
                .await?
            {
                Some(m) => Ok(m.id),
                None => MoveActiveModel {
                    name: Set(m.name.clone()),
                    ..Default::default()
                }
                .insert(db)
                .await
                .map(|res| res.id),
            }
        });

        let move_ids = try_join_all(move_futures).await?;

        // Handling types
        for type_id in type_ids {
            let _ = PokemonTypeActiveModel {
                pokemon_id: Set(pokemon.id),
                type_id: Set(type_id),
            }
            .insert(&txn)
            .await?;
        }

        for move_id in move_ids {
            let _ = PokemonMoveActiveModel {
                pokemon_id: Set(pokemon.id),
                move_id: Set(move_id),
            }
            .insert(&txn)
            .await?;
        }

        txn.commit().await?;

        Ok(PokemonWithDetails {
            id: Some(pokemon.id),
            name: form_data.name.clone(),
            description: form_data.description.clone(),
            front_sprite_url: form_data.front_sprite_url.clone(),
            back_sprite_url: form_data.back_sprite_url.clone(),
            front_shiny_sprite_url: form_data.front_shiny_sprite_url.clone(),
            back_shiny_sprite_url: form_data.back_shiny_sprite_url.clone(),
            types: form_data.types.clone(),
            moves: form_data.moves.clone(),
            last_sync: pokemon.last_sync.clone(),
        })
    }

    pub async fn update_pokemon_by_id(
        db: &DbConn,
        id: i32,
        form_data: PokemonWithDetails,
    ) -> Result<PokemonModel, DbErr> {
        let pokemon: PokemonActiveModel = Pokemon::find_by_id(id)
            .one(db)
            .await?
            .ok_or(DbErr::Custom("Cannot find pokemon.".to_owned()))
            .map(Into::into)?;

        PokemonActiveModel {
            id: pokemon.id,
            name: Set(form_data.name.to_owned()),
            description: Set(form_data.description.to_owned()),
            front_sprite_url: Set(form_data.front_sprite_url.to_owned()),
            back_sprite_url: Set(form_data.back_sprite_url.to_owned()),
            front_shiny_sprite_url: Set(form_data.front_shiny_sprite_url.to_owned()),
            back_shiny_sprite_url: Set(form_data.back_shiny_sprite_url.to_owned()),
            last_sync: Set(now()),
        }
        .update(db)
        .await
    }

    pub async fn delete_pokemon_by_id(db: &DbConn, id: i32) -> Result<DeleteResult, DbErr> {
        let pokemon: PokemonActiveModel = Pokemon::find_by_id(id)
            .one(db)
            .await?
            .ok_or(DbErr::Custom("Cannot find pokemon.".to_owned()))
            .map(Into::into)?;

        pokemon.delete(db).await
    }

    pub async fn delete_all_pokemon(db: &DbConn) -> Result<DeleteResult, DbErr> {
        Pokemon::delete_many().exec(db).await
    }
}
