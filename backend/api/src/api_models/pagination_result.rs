use rocket::serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Deserialize, Serialize, FromForm)]
#[serde(crate = "rocket::serde")]
pub struct PaginationResult<T> {
    pub items: Vec<T>,
    pub total_items: u64,
    /// 0-based index
    pub page: u64,
    pub page_size: u64,
    pub num_pages: u64,
}
