#[macro_use]
extern crate rocket;

use rocket::fairing::{self, AdHoc};
use rocket::{Build, Request, Rocket};
use serde_json::json;

use migration::MigratorTrait;
use sea_orm_rocket::{Connection, Database};

mod v1;

mod api_models;
mod pool;

use pool::Db;

#[get("/health")]
fn index() -> &'static str {
    "Healthy"
}

#[catch(404)]
pub fn not_found(req: &Request<'_>) -> &'static str {
    return "Not Found!";
}

async fn run_migrations(rocket: Rocket<Build>) -> fairing::Result {
    let conn = &Db::fetch(&rocket).unwrap().conn;
    let _ = migration::Migrator::up(conn, None).await;
    Ok(rocket)
}

#[tokio::main]
async fn start() -> Result<(), rocket::Error> {
    rocket::build()
        .attach(Db::init())
        .attach(AdHoc::try_on_ignite("Migrations", run_migrations))
        .register("/", catchers![not_found])
        .attach(v1::stage())
        .launch()
        .await
        .map(|_| ())
}

pub fn main() {
    let result = start();

    println!("Rocket: deorbit.");

    if let Some(err) = result.err() {
        println!("Rocket: error: {:?}", err);
    }
}
