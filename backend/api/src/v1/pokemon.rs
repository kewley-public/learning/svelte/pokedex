use rocket::fairing::{self, AdHoc};
use rocket::form::{Context, Form};
use rocket::fs::{relative, FileServer};
use rocket::request::FlashMessage;
use rocket::response::{Flash, Redirect};
use rocket::serde::json::Json;
use rocket::{Build, Request, Rocket};
use serde_json::json;
use service::{Mutation, Query};

use migration::MigratorTrait;
use models::prelude::*;
use sea_orm_rocket::{Connection, Database};

use crate::api_models::PaginationResult;
use crate::pool::Db;

const DEFAULT_POKEMON_PER_PAGE: u64 = 5;

#[post("/", data = "<pokemon_request>")]
async fn create_pokemon(
    conn: Connection<'_, Db>,
    pokemon_request: Json<CreatePokemonRequest>,
) -> Flash<Json<PokemonWithDetails>> {
    let db = conn.into_inner();

    let form = pokemon_request.into_inner();

    let new_pokemon = Mutation::create_pokemon(db, form)
        .await
        .expect("could not insert pokemon");

    Flash::success(Json(new_pokemon), "Pokemon successfully added.")
}

#[get("/<id>")]
async fn get_pokemon_by_id(id: i32, conn: Connection<'_, Db>) -> Option<Json<PokemonWithDetails>> {
    let db = conn.into_inner();

    Query::find_pokemon_by_id(db, id)
        .await
        .ok()
        .flatten()
        .map(Json)
}

#[get("/?<page>&<page_size>")]
async fn list(
    conn: Connection<'_, Db>,
    page: Option<u64>,
    page_size: Option<u64>,
) -> Json<PaginationResult<PaginatedPokemon>> {
    let db = conn.into_inner();

    // Set page number and items per page
    let page = page.unwrap_or(1);
    let page_size = page_size.unwrap_or(DEFAULT_POKEMON_PER_PAGE);
    if page == 0 {
        panic!("Page number cannot be zero");
    }

    let (items, num_pages, total_items) = Query::find_pokemon_in_page(db, page, page_size)
        .await
        .expect("Cannot find pokemon in page");

    Json(PaginationResult {
        page,
        page_size,
        num_pages,
        total_items,
        items,
    })
}

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("Pokemon V1 Stage", |rocket| async {
        rocket.mount(
            "/api/v1/pokemon",
            routes![create_pokemon, get_pokemon_by_id, list],
        )
    })
}
