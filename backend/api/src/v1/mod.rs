use rocket::fairing::AdHoc;

mod pokemon;

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("V1 Stage", |rocket| async {
        rocket.attach(pokemon::stage())
    })
}
