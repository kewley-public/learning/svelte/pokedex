# Pokemon!

Simple application that utilizes the [poke api](https://pokeapi.co/) to display a list of pokemon and allows you to
save favorites to view.

## Environment

Copy the `.env.example` into a new file called `.env`

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`),
start a development server:

```bash
npm run dev

# or start the server and open the app in a new browser tab
npm run dev -- --open
```

## Building

To create a production version of your app:

```bash
npm run build
```

You can preview the production build with `npm run preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.

## Docker

We do have a `docker-compose.yml` file to run the application in a docker container to simulate
what a live production build will be like.

```shell
docker compose up -d --build
```

Shut it down

```shell
docker compose down
```

## Deployment

We utilize fly.io for our deployment process. You might also see sst in here but for now that is legacy since SST
is migrating to a newer version which I'm not really a big fan of.

### How

#### Only Needed once

Create the app:

```shell
fly apps create pokemon-svelte
```

Then create a single volume

```shell
fly volumes create data_volume --size 1 --app pokemon-svelte -r ord
```

Create the DATABASE_URL secret

```shell
fly secrets set DATABASE_URL="file:/data/dev.db"
```

Now run the deployment

```shell
fly deploy
```

SSH into the instance

```shell
# Sets up the SSH (only needs to be run once)
flyctl ssh issue --agent
```

```shell
flyctl ssh console
```

Run the DB migration

```shell
npx run prisma migrate deploy
```

#### Subsequent Deployments

Simple as just running

```shell
fly deploy
```
