import { json } from '@sveltejs/kit';
import { env } from '$env/dynamic/private';
import axios from 'axios';

const POKEMON_ENDPOINT = `${env.BASE_BACKEND_URL}/api/v1/pokemon`;

export async function GET({ url }): Promise<ReturnType<typeof json>> {
	const idMatch = url.pathname.match(/\/my-pokedex\/(\d+)/);
	const isSingleItem = idMatch !== null;

	if (isSingleItem) {
		const id = idMatch[1];
		const singleEndpoint = `${POKEMON_ENDPOINT}/${id}`;
		try {
			console.info('Fetching data from the backend singleEndpoint:', singleEndpoint);
			const result = await axios.get(singleEndpoint);
			console.info(result.data.name);
			return json(result.data, {});
		} catch (e) {
			console.error(e);
			return json({ error: 'Failed to fetch Pokémon' }, { status: 500 });
		}
	} else {
		const page = url.searchParams.get('page');
		const pageSize = url.searchParams.get('page_size');
		const listEndpoint = `${POKEMON_ENDPOINT}?page=${page}&page_size=${pageSize}`;
		try {
			console.info('Fetching data from the backend listEndpoint:', listEndpoint);
			const result = await axios.get(listEndpoint);
			return json(result.data, {});
		} catch (e) {
			console.error(e);
			return json({ error: 'Failed to fetch Pokémon list' }, { status: 500 });
		}
	}
}

export async function POST() {
	return json({});
}
