import { backendApi } from '$lib/api';

export async function load({ fetch, params }) {
	return {
		pokemon: backendApi(fetch).pokemonDetails(params.slug)
	};
}
