import { backendApi } from '$lib/api';

export async function load({ fetch, depends }) {
	depends('app:pokemon');
	return {
		listResults: backendApi(fetch).myPokemon()
	};
}
