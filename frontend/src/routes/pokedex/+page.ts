import { api } from '$lib/api';

export async function load({ parent }) {
	const { queryClient } = await parent();

	await queryClient.prefetchQuery({
		queryKey: ['pokemon', 10],
		queryFn: () => api().loadPokemonData(10)
	});
}
