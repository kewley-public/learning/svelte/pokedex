export interface PaginatedResults<T> {
	items: T[];
	total_items: number;
	/// 0-based index
	page: number;
	page_size: number;
	num_pages: number;
}

export interface PokemonItem {
	name: string;
	url: string;
}

export interface Type {
	id: number | null;
	name: string;
}

export interface Move {
	id: number | null;
	name: string;
}

export type UTC = string; // ISO date string, since TypeScript doesn't have a native NaiveDateTime equivalent

export interface CreatePokemonRequest {
	name: string;
	description: string;
	front_sprite_url: string;
	back_sprite_url: string;
	front_shiny_sprite_url: string;
	back_shiny_sprite_url: string;
	types: Type[];
	moves: Move[];
}

export interface PaginatedPokemon {
	id: number | null;
	name: string;
	description: string;
	front_sprite_url: string;
	back_sprite_url: string;
	front_shiny_sprite_url: string;
	back_shiny_sprite_url: string;
	last_sync: UTC; // Treat as an ISO date string
}

// Translated TypeScript interface
export interface PokemonWithDetails {
	id: number | null; // Rust's `Option<i32>` can be represented as `number | null`
	name: string;
	description: string;
	types: Type[];
	moves: Move[];
	front_sprite_url: string;
	back_sprite_url: string;
	front_shiny_sprite_url: string;
	back_shiny_sprite_url: string;
	last_sync: UTC; // Treat as an ISO date string
}

export const EMPTY_POKEMON: PokemonWithDetails = {
	name: 'Missing No.',
	id: -1,
	description: '',
	types: [],
	moves: [],
	front_sprite_url: '',
	back_sprite_url: '',
	front_shiny_sprite_url: '',
	back_shiny_sprite_url: '',
	last_sync: '1970-01-01T00:00:00Z'
};
