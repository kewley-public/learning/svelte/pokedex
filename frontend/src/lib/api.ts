import { PokemonClient } from 'pokenode-ts';
import { error } from '@sveltejs/kit';

const pokemonClient = new PokemonClient();

export const api = () => ({
	loadPokemonData: async (limit: number, offset: number = 0) => {
		const response = await pokemonClient.listPokemons(offset, limit);
		return {
			items: response.results,
			total_items: response.count,
			page: offset,
			page_size: limit,
			num_pages: Math.ceil(response.count / limit)
		};
	},
	getPokemonByName: async (name: string) => await pokemonClient.getPokemonByName(name)
});

export const backendApi = (customFetch = fetch) => ({
	myPokemon: async (limit: number = 10, offset: number = 1) => {
		const resp = await customFetch(`/api/my-pokedex?page=${offset}&page_size=${limit}`);
		if (resp.ok) {
			return await resp.json();
		}
		error(400, 'Unknown error');
	},
	pokemonDetails: async (id: number) => {
		const resp = await customFetch(`/api/my-pokedex/${id}`);
		if (resp.ok) {
			return await resp.json();
		}
		error(400, 'Unknown error');
	}
});
